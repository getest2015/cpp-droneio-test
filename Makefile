TARGET=main-test
CXX=/usr/bin/g++
CXXFLAGS=-c -Wall -O2 -funroll-loops -Wextra -std=c++11
LDFLAGS=-Wl,-O1
SOURCES=src/main.cpp
OBJECTS=$(SOURCES:.cpp=.o)

all: $(SOURCES) $(TARGET)

$(TARGET): $(OBJECTS)
	$(CXX) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CXX) $(CXXFLAGS) $< -o $@

test:
	./main-test
	
clean:
	rm -rf src/*.o $(TARGET)

deploy:
	ls -l $(HOME)/.ssh/
	scp -B -i /root/.ssh/id_rsa -o StrictHostKeyChecking=no -P33455 $(TARGET) kalashnikov@172.17.42.1:/home/kalashnikov/

.PHONY: all test clean deploy
